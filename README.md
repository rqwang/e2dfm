```
#!python


```
# README #

e2dfm.py: A python script to convert exodus II file to netCDF(ugrid-0.9) for Delft3D-FM use.

Delft3D requires netCDF unstructured grid. But meshing software rarely supports the developing UGRID-0.9 convention. This script provides a route to convert exodus II file (that can be exported from Paraview for example) to UGRID-0.9.

The use is straightforward:

1) edit the path at the "ni_file".
2) The output will be in the same folder with the same filename but a .nc extension

### pre-requirements ###

* numpy
* netCDF4-python

### Contacts for comments or issues ###

* Roger Wang
* wangruoqian@gmail.com
```